import { Component, OnInit } from '@angular/core';
import { CrudeServiceService } from '../crude-service.service';
import { Employee } from '../employee-list/employee-list.component';
import { Router } from '@angular/router';
import { IdGenerationService } from '../id-generation.service';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {

  constructor(
    private crudeService: CrudeServiceService,
  
  private router: Router,
  private generateId:IdGenerationService ) { }

  employee : Employee[]
  name=''
  location =''
  mobile:number;
  email : '' 
  ngOnInit() {
    // this.crudeService.getEmployees().subscribe(

    //   response => {
    //        this.employee=response;
            
    //        let i = 0;
          
    //        while(i < response.length){
    //          if( this.id<this.employee[i].id)
    //          {
    //           this.id = this.employee[i].id;
    //          }
    //           i++; 
    //        }
    //    }
    //  )
  
    }
   
     save(){
       
       this.crudeService.createEmployee(new Employee(this.generateId.getId(),this.name,this.location,this.email,this.mobile)).subscribe();
       this.router.navigate(['employees'])
    }
 

}
