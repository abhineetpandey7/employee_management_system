import { Component, OnInit } from '@angular/core';
import { CrudeServiceService } from '../crude-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Employee } from '../employee-list/employee-list.component';

@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.css']
})
export class EmployeeDetailsComponent implements OnInit {

  constructor(private crudeService: CrudeServiceService,
    private route:ActivatedRoute,
    private router:Router) { }
   id : number
   employee : Employee
  

  ngOnInit() {

    this.id = this.route.snapshot.params['id']
    
    this.crudeService.getEmployeeById(this.id).subscribe(
 
    response => {
      this.employee = response
     }
   )
  }

  routingTo(){
    this.router.navigate(['employees'])
  }

}
