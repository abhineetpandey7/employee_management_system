import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class IdGenerationService {
   id = 3;
  constructor() { }

  getId(){
    this.id = this.id + 1;
    return (this.id);
  }
}
