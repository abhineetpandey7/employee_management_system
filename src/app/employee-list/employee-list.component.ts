import { Component, OnInit } from '@angular/core';
import { CrudeServiceService } from '../crude-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})

export class EmployeeListComponent implements OnInit {

  constructor(private crudeService:CrudeServiceService,
               private router:Router) { }
  employees : Employee[];
  allEmployees : Employee[];
  filter = ''
  empLength:number;
  ngOnInit() {
   this.refreshEmployee();
  }

  refreshEmployee(){
    this.crudeService.getEmployees().subscribe(
      response => {
        this.employees = response;
        this.allEmployees = response;
        this.empLength = response.length
      }
     )
  }
  applyFilter() {
    if(this.filter === '' ) {
        this.employees=this.allEmployees;
        this.empLength = this.employees.length;
    } 
    else {
      this.crudeService.getEmployees().subscribe(
        response => {
          let count = 0;
          this.allEmployees = response;
          this.employees.splice(0,this.allEmployees.length);
         for(let i=0;i<this.allEmployees.length;i++){    
           if(this.allEmployees[i].name.toLowerCase().startsWith(this.filter.toLowerCase())){
             this.employees[count++] = this.allEmployees[i];
           }
            
          }
          this.empLength = this.employees.length;
                   
           
        })
    }
 }
 deleteById(empId:number){
   this.crudeService.deleteEmployee(empId).subscribe();
   this.refreshEmployee();
 }
 edit(id){
   this.router.navigate(['editEmployee',id])
 }


}
export class Employee{
  constructor(public id:number,public name:string,public location:string,public email:string,public mobile:number){}
}

