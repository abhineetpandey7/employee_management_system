import { TestBed } from '@angular/core/testing';

import { CrudeServiceService } from './crude-service.service';

describe('CrudeServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CrudeServiceService = TestBed.get(CrudeServiceService);
    expect(service).toBeTruthy();
  });
});
