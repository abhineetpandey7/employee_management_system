import { Component, OnInit } from '@angular/core';
import { CrudeServiceService } from '../crude-service.service';
import { Employee } from '../employee-list/employee-list.component';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-employee',
  templateUrl: './edit-employee.component.html',
  styleUrls: ['./edit-employee.component.css']
})
export class EditEmployeeComponent implements OnInit {

  constructor(private crudeService: CrudeServiceService,
    private route:ActivatedRoute,
    private router:Router) { }
   id : number
   employee : Employee
   name = ''
  
   ngOnInit() {
   this.id = this.route.snapshot.params['id']
    this.employee = new Employee(-1,'','','',0);
   this.crudeService.getEmployeeById(this.id).subscribe(

   response => {
     this.employee = response
     this.name = this.employee.name;
 
    }
  )
   
  }

  updateEmployee(){
    this.crudeService.updateEmployee(this.employee).subscribe();
    this.router.navigate(['employees'])
  }

}

