import { TestBed } from '@angular/core/testing';

import { IdGenerationService } from './id-generation.service';

describe('IdGenerationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IdGenerationService = TestBed.get(IdGenerationService);
    expect(service).toBeTruthy();
  });
});
