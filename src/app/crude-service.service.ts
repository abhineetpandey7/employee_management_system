import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Employee } from './employee-list/employee-list.component';

@Injectable({
  providedIn: 'root'
})
export class CrudeServiceService {

  constructor(private http:HttpClient) { }
  SERVER_URL = 'http://localhost:8080/api/'
    getEmployees(){
    return this.http.get<Employee[]>(this.SERVER_URL+'employees')
  }

 deleteEmployee(empId){
    return this.http.delete(`${this.SERVER_URL + 'employees'}/${empId}`)
}
getEmployeeById(empId){
  console.log(`${this.SERVER_URL + 'employees'}/${empId}`)
  return this.http.get<Employee>(`${this.SERVER_URL + 'employees'}/${empId}`);
}

public updateEmployee(employee : Employee){
  return this.http.put(`${this.SERVER_URL + 'employees'}/${employee.id}`, employee)
}
public createEmployee(employee: {id: number, name:string,location:string,email:string,mobile:number}){
  return this.http.post(`${this.SERVER_URL + 'employees'}`, employee)
}

}
